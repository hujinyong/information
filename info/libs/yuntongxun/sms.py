#!/usr/bin/env python
# -*- coding:utf-8 -*-
from info.libs.yuntongxun.CCPRestSDK import REST


# 说明：主账号，登陆云通讯网站后，可在"控制台-应用"中看到开发者主账号ACCOUNT SID
accountSid = '8aaf07086b2bcfcf016b30fed125012b'

# 说明：主账号Token，登陆云通讯网站后，可在控制台-应用中看到开发者主账号AUTH TOKEN
accountToken = 'b64b7565af6449329590828545fa717a'

# 请使用管理控制台首页的APPID或自己创建应用的APPID
appId = '8aaf07086b2bcfcf016b30fed18b0132'

# 说明：请求地址，生产环境配置成app.cloopen.com
serverIP = 'app.cloopen.com'

# 说明：请求端口 ，生产环境为8883
serverPort = '8883'

# 说明：REST API版本号保持不变
softVersion = '2013-12-26'


class CCP(object):
    def __new__(cls, *args, **kwargs):
        if not hasattr(CCP,"instance"):
            cls.instance = super(CCP, cls).__new__(cls, *args, **kwargs)
            cls.instance.rest = REST(serverIP,serverPort,softVersion)
            cls.instance.rest.setAccount(accountSid,accountToken)
            cls.instance.rest.setAppId(appId)
        return cls.instance


    def send_template_sms(self,to,datas,temp_id):
        """
        :param to: 手机号
        :param datas: 短信内容
        :param temp_id: 模板Id
        :return:
        """
        result = self.rest.sendTemplateSMS(to,datas,temp_id)
        print(result)
        if result.get("statusCode")=='000000':
            return 0
        else:
            return -1

