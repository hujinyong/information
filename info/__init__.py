import logging
from logging.handlers import RotatingFileHandler

from flask import Flask
from flask import g
from flask import render_template
from flask.ext.session import Session
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.wtf import CSRFProtect
from flask.ext.wtf.csrf import generate_csrf
from redis import StrictRedis
from config import *


# 声明创建db
db = SQLAlchemy()
# 创建redis_stock对象为None ，在create_app 函数中修改全局变量的值
redis_store = None  # type: StrictRedis


def setup_log(config_name):
    """配置日志"""

    # 设置日志的记录等级
    logging.basicConfig(level=config_dict[config_name].LOG_LEVEL)  # 调试debug级
    # 创建日志记录器，指明日志保存的路径、每个日志文件的最大大小、保存的日志文件个数上限 ,路径必须是绝对路径！！！
    file_log_handler = RotatingFileHandler("logs/log",maxBytes=1024 * 1024 * 100, backupCount=10)
    # 创建日志记录的格式 日志等级 输入日志信息的文件名 行数 日志信息
    formatter = logging.Formatter('%(levelname)s %(filename)s:%(lineno)d %(message)s')
    # 为刚创建的日志记录器设置日志记录格式
    file_log_handler.setFormatter(formatter)
    # 为全局的日志工具对象（flask app使用的）a添加日志记录器
    logging.getLogger().addHandler(file_log_handler)


def create_app(config_name):
    # 调用日志函数
    setup_log(config_name)

    # 创建app对象
    app = Flask(__name__)
    # 将app对象和数据库绑定并初始化
    app.config.from_object(config_dict[config_name])

    db.init_app(app)
    # 声明全局变量redis_stock
    global redis_store
    redis_store = StrictRedis(host=config_dict[config_name].HOST_IP, port=config_dict[config_name].PORT,
                              decode_responses=True)
    # 设置session保存在redis数据库中
    Session(app)
    # 设置CSRF验证保护 ,只做服务器验证功能
    CSRFProtect(app)

    # 注册自定义过滤器
    from info.utils.common import do_index_class
    app.add_template_filter(do_index_class, "index_class")

    # 抓取404报错
    from info.utils.common import user_login_data

    @app.errorhandler(404)
    @user_login_data
    def page_not_found(e):
        user = g.user
        data = {"user_info": user.to_dict() if user else None}
        return render_template("news/404.html", data=data)

    # 在请求钩子里给cookie放入CSRF保护的密文以便与请求头里的csrf_token做校验来开启CSRFProtect
    @app.after_request
    def after_request(response):
        csrf_token = generate_csrf()
        response.set_cookie("csrf_token", csrf_token)
        return response

    # 需要在注册蓝图的时候才导入，否则会循环导入而报错
    from .modules.index import index_blu
    # 将创建的蓝图视图注册到app上
    app.register_blueprint(index_blu)

    from .modules.passport import passport_blu
    app.register_blueprint(passport_blu)

    from .modules.news import news_blu
    app.register_blueprint(news_blu)

    from .modules.profile import profile_blu
    app.register_blueprint(profile_blu)

    from .modules.admin import admin_blu
    app.register_blueprint(admin_blu)

    # 返回生成的app对象
    return app
