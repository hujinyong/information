#!/usr/bin/env python
# -*- coding:utf-8 -*-
from flask import current_app
from flask import g
from flask import session
import functools
from info.models import User


def do_index_class(index):
    if index == 0:
        return "first"
    elif index == 1:
        return "second"
    elif index == 2:
        return "third"
    else:
        return ""


def user_login_data(func):
    @functools.wraps(func)
    def call_func(*args, **kwargs):
        user_id = session.get("user_id", None)
        user = None
        if user_id:
            try:
                user = User.query.get(user_id)
            except Exception as e:
                current_app.logger.error(e)
        g.user = user
        return func(*args, **kwargs)
    return call_func
