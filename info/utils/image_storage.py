#!/usr/bin/env python
# -*- coding:utf-8 -*-
import logging
from qiniu import Auth, put_data

from info import constants

access_key = constants.ACCESS_KEY
secret_key = constants.SECRET_KEY
bucket_name = constants.IHOME


def storage(data):
    if not data:
        return None
    try:
        # 构建鉴权对象
        q = Auth(access_key, secret_key)
        # 生成上传Token，可以指定过期时间等
        token = q.upload_token(bucket_name)
        # 上传文件
        ret, info = put_data(token, None, data)

    except Exception as e:
        logging.error(e)
        raise e

    if info and info.status_code != 200:
        raise Exception("上传文件到七牛失败")

    return ret["key"]
