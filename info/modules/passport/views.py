#!/usr/bin/env python
# -*- coding:utf-8 -*-
import random
import re
from datetime import datetime

from flask import abort, jsonify
from flask import current_app
from flask import make_response
from flask import request
from flask import session

from info.libs.yuntongxun.sms import CCP
from info.models import User
from  info.utils.captcha.captcha import captcha
from info import redis_store, constants, db
from info.utils.response_code import RET

from . import passport_blu


@passport_blu.route("/logout")
def logout():
    session.pop("user_id", None)
    session.pop("nick_name", None)
    session.pop("mobile", None)
    # 清除is_admin属性，避免管理员登录后其他正常用户也能登录后台的情况
    session.pop("is_admin",None)

    return jsonify(errno=RET.OK, errmsg="退出登录成功")


@passport_blu.route("/login", methods=["post"])
def login():
    """
    1.获取数据
    2.校验数据
    3.根据手机号查询用户，
    4.校验用户输入的密码与查询到的密码看是否相同
    5.相同保存用户状态并响应

    6.记录用户最后的登录时间
    :return:
    """
    params_dict = request.json
    mobile = params_dict.get("mobile")
    password = params_dict.get("password")

    if not all([mobile, password]):
        return jsonify(errno=RET.NODATA, errmsg="参数不全")

    # 校验手机号是否正确
    if not re.match('1[35678]\\d{9}', mobile):
        return jsonify(errno=RET.PARAMERR, errmsg="手机号格式不正确")
    # 3.根据用户手机号查询用户
    try:
        user = User.query.filter(User.mobile == mobile).first()
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DATAERR, errmsg="查询错误")
    if not user:
        return jsonify(errno=RET.NODATA, errmsg="用户不存在")
    # 4通过调用USER的check_password方法做密码验证
    if not user.check_password(password):
        return jsonify(errno=RET.PWDERR, errmsg="用户名或者密码错误")
    # 5.相同保存用户状态并响应
    session["user_id"] = user.id
    session["mobile"] = user.mobile
    session["nick_name"] = user.nick_name

    # 记录用户最后一次的登录时间
    user.last_login = datetime.now()
    # 把SQLALCHEMY_COMMIT_ON_TEARDOWN = True之后会自动提交，有一定局限性
    # try:
    #     db.session.commit()
    # except Exception as e:
    #     current_app.logger.error(e)
    return jsonify(errno=RET.OK, errmsg="登录成功")


@passport_blu.route("/register", methods=["POST"])
def register():
    """
    1.获取参数
    2.校验参数
    2.1 查询手机号是否被注册
    3.从数据库中取出短信验证码与传过来的短信验证码进行比对
    4.校验通过创建user对象并存入数据库
    5.把创建后的用户改为登陆状态，并且在session中保存信息
    :return:
    """
    # 1 .获取参数
    param_dict = request.json
    mobile = param_dict.get("mobile")
    smscode = param_dict.get("smscode")
    password = param_dict.get("password")
    # 2.校验参数
    if not all([mobile, smscode, password]):
        return jsonify(errno=RET.PARAMERR, errmsg="参数不全")
    # 2.1查询手机号是否被注册
    try:
        result = User.query.filter(User.mobile == mobile).first()
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="数据查询失败")
    if result:
        return jsonify(errno=RET.DATAEXIST, errmsg="手机号已注册")

    # 3.从数据库中取出短信验证码与传过来的短信验证码进行比对
    try:
        real_sms_code = redis_store.get("SMS_" + mobile)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="数据查询失败")
    # 如果查询不到数据，说明验证码已经过期
    if not real_sms_code:
        return jsonify(errno=RET.NODATA, errmsg="验证码已过期")
    # 将页面中的验证码去空格后和redis中的短信验证码进行比对
    if smscode.strip() != real_sms_code:
        return jsonify(reeno=RET.DATAERR, errmsg="验证码错误")
    # 4校验通过创建user对象并存入数据库
    user = User()
    user.mobile = mobile
    user.nick_name = mobile
    user.password = password
    try:
        db.session.add(user)
        db.session.commit()
    except Exception as e:
        db.session.rollback()
        current_app.logger.error(e)
        return jsonify(errno=RET.DATAERR, errmsg="数据保存失败")
    # 5.注册成功后将用户的状态改为登陆状态
    session["user_id"] = user.id
    session["mobile"] = user.mobile
    session["nick_name"] = user.nick_name
    return jsonify(errno=RET.OK, errmsg="注册成功")


@passport_blu.route('/sms_code', methods=["POST"])
def send_sms():
    """
    1. 接收参数并判断是否有值
    2. 校验手机号是正确
    3. 通过传入的图片编码去redis中查询真实的图片验证码内容
    4. 进行验证码内容的比对
    5. 生成发送短信的内容并发送短信
    6. redis中保存短信验证码内容
    7. 返回发送成功的响应
    :return:
    """
    params_dict = request.json
    mobile = params_dict.get("mobile")
    image_code = params_dict.get("image_code")
    image_code_id = params_dict.get("image_code_id")

    # 1校验是否都有值
    if not all([mobile, image_code, image_code_id]):
        return jsonify(errno=RET.PARAMERR, errmsg="参数不全")
    # 2校验手机号
    if not re.match("^1[3578][0-9]{9}", mobile):
        return jsonify(errno=RET.DATAERR, errmsg="手机号不正确")
    # 3通过传入的图片编码去redis中查询真实的图片验证码内容
    try:
        real_image_code = redis_store.get("ImageCodeId_" + image_code_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="数据查询失败")

    if not real_image_code:
        return jsonify(errno=RET.NODATA, errmsg="图片验证码已过期")

    # 4与用户的验证码内容进行对比，如果对比不一致，那么返回验证码输入错误
    if real_image_code.upper() != image_code.upper():
        return jsonify(errno=RET.DATAERR, errmsg="验证码输入错误")

    # 5. 如果一致，生成短信验证码的内容(随机数据)
    # 随机数字 ，保证数字长度为6位，不够在前面补上0
    sms_code_str = "%06d" % random.randint(0, 999999)
    current_app.logger.debug("短信验证码内容是：%s" % sms_code_str)
    # 6 发送验证码
    result = CCP().send_template_sms(mobile, [sms_code_str, constants.SMS_CODE_REDIS_EXPIRES / 5], "1")
    print(result)
    if result != 0:
        # print(result)
        return jsonify(errno=RET.THIRDERR, errmsg="发送短信失败")
    # # 保存验证码到redis
    try:
        redis_store.set("SMS_" + mobile, sms_code_str, constants.SMS_CODE_REDIS_EXPIRES)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="数据保存失败")

    # 7告知发送结果
    return jsonify(errno=RET.OK, errmsg="发送成功")


@passport_blu.route('/image_code')
def get_image_code():
    """生成图片验证码并返回"""
    # 1. 取得参数
    image_code_id = request.args.get("imageCodeId", None)
    # 2.判断是否有值
    if not image_code_id:
        return abort(403)
    # 3.生成图片验证码
    name, text, image = captcha.generate_captcha()

    # 4.保存图片验证码的文字内容到redis
    try:
        redis_store.set("ImageCodeId_" + image_code_id, text, ex=constants.IMAGE_CODE_REDIS_EXPIRES)
    except Exception as e:
        current_app.logger.error(e)
        abort(500)

    # 5 返回生成的验证码图片
    response = make_response(image)
    # 设置数据类型以便兼容浏览器
    response.headers["Content-Type"] = "image/jpg"
    return response
