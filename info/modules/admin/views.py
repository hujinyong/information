#!/usr/bin/env python
# -*- coding:utf-8 -*-
import time

import datetime
from flask import current_app, jsonify
from flask import g
from flask import redirect
from flask import render_template
from flask import request
from flask import session
from flask import url_for

from info import constants, db
from info.models import User, News, Category
from info.utils.common import user_login_data
from info.utils.image_storage import storage
from info.utils.response_code import RET
from . import admin_blu


@admin_blu.route("/logout")
def logout():

    session.pop("user_id",None)
    session.pop("nick_name",None)
    session.pop("is_admin",None)
    return redirect(url_for("index.index"))


@admin_blu.route("/news_type", methods=["get", "post"])
def get_news_category():
    if request.method == "GET":
        try:
            categories = Category.query.all()
        except Exception as e:
            current_app.logger.error(e)
            return render_template("admin/news_type.html", errmsg="数据查询失败")
        categories_li = []
        for category in categories:
            categories_li.append(category.to_dict())
        categories_li.pop(0)
        data = {
            "categories": categories_li
        }
        return render_template("admin/news_type.html", data=data)
    else:
        # 保存分类修改
        category_id = request.json.get("id")
        category_name = request.json.get("name")
        if not category_name:
            return jsonify(errno=RET.PARAMERR, errmsg="参数缺少")
        # 判断是否有分类id
        if category_id:
            try:
                category = Category.query.get(category_id)
            except Exception as e:
                current_app.logger.error(e)
                return jsonify(errno=RET.DBERR, errmsg="数据查询失败")
            if not category:
                return jsonify(errno=RET.NODATA, errmsg="未查询到数据")
            category.name = category_name
        else:
            category = Category()
            category.name = category_name
            db.session.add(category)

        return jsonify(errno=RET.OK, errmsg="保存分类成功")


@admin_blu.route("/news_edit_detail", methods=["get", "post"])
def news_edit_detail():
    """新闻编辑详情"""
    if request.method == "GET":
        news_id = request.args.get("news_id")
        if not news_id:
            return render_template("admin/news_edit_detail.html", errmsg="用户未登录")
        if not news_id:
            return render_template("admin/news_edit_detail.html", errmsg="未查询到此新闻")
        news = None
        try:
            news = News.query.get(news_id)
        except Exception as e:
            current_app.logger.error(e)
            return render_template("admin/news_edit_detail.html", errmsg="数据库查询错误")

        if not news:
            return render_template("admin/news_edit_detail.html", errmsg="未查询到此新闻")
        # 查询分类数据
        categories = Category.query.all()
        categories_li = []
        for category in categories:
            c_dict = category.to_dict()
            c_dict["is_selected"] = False
            if category.id == news.category_id:
                c_dict["is_selected"] = True
            categories_li.append(c_dict)

        # 移除‘最新’分类
        categories_li.pop(0)
        data = {
            "news": news.to_dict(),
            "categories": categories_li
        }

        return render_template("admin/news_edit_detail.html", data=data)
    else:
        news_id = request.form.get("news_id")
        title = request.form.get('title')
        digest = request.form.get("digest")
        content = request.form.get("content")
        index_image = request.form.get("index_image")
        category_id = request.form.get("category_id")

    if not all([news_id, title, digest, content, category_id]):
        return jsonify(errno=RET.PARAMERR, errmsg="参数缺少")
    news = None
    try:
        news = News.query.get(news_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="数据库查询错误")

    if not news:
        return jsonify(errno=RET.NODATA, errmsg="未查询到数据")
    # 尝试读取图片
    if index_image:
        try:
            index_image = index_image.read()
        except Exception as e:
            current_app.logger.error(e)
            return jsonify(errno=RET.PARAMERR, errmsg="图片读取失败")
        # 将图片上传到七牛云
        try:
            key = storage(index_image)
        except Exception as e:
            current_app.logger.error(e)
            return jsonify(errno=RET.THIRDERR, errmsg="上传图片错误")
        news.index_image_url = constants.QINIU_DOMIN_PREFIX + key
    # 设置相关数据
    news.title = title
    news.digest = digest
    news.content = content
    news.category_id = category_id

    # 设置了sqlalchemy_commin_on_theardown不用保存数据库
    return jsonify(errno=RET.OK, errmsg="编辑成功")


@admin_blu.route("/news_edit")
def news_edit():
    """返回新闻列表"""
    page = request.args.get("p", 1)
    keywords = request.args.get("keywords", "")
    try:
        page = int(page)
    except Exception as e:
        current_app.logger.error(e)
        page = 1

    news_list = []
    current_page = 1
    total_page = 1

    try:
        filters = [News.status == 0]
        if keywords:
            # 添加关键字搜索选项
            filters.append(News.title.contains(keywords))
        paginate = News.query.filter(*filters).order_by(News.create_time.desc()).paginate(page,
                                                                                          constants.ADMIN_NEWS_PAGE_MAX_COUNT,
                                                                                          False)
        news_list = paginate.items
        current_page = paginate.page
        total_page = paginate.pages
    except Exception as e:
        current_app.logger.error(e)

    news_dict_li = []
    for news in news_list:
        news_dict_li.append(news.to_review_dict())
    data = {
        "news_list": news_dict_li,
        "current_page": current_page,
        "total_page": total_page
    }
    return render_template("admin/news_edit.html", data=data)


@admin_blu.route("/news_review_detail", methods=["get", "post"])
def news_review_detail():
    """新闻的审核按钮功能"""
    if request.method == "GET":
        news_id = request.args.get("news_id")
        if not news_id:
            return render_template("admin/news_review_detail.html", data={"errmsg": "未查询到此新闻信息"})
        news = None
        try:
            news = News.query.get(news_id)
        except Exception as e:
            current_app.logger.error(e)

        if not news:
            return render_template("admin/news_review_detail.html", data={"errmsg": "未查询的此新闻"})

        data = {
            "news": news.to_dict()
        }
        return render_template("admin/news_review_detail.html", data=data)
    else:
        # POST请求时候的逻辑
        news_id = request.json.get("news_id")
        action = request.json.get("action")
        if not all([news_id, action]):
            return jsonify(errno=RET.PARAMERR, errmsg="缺少参数")
        if action not in ["accept", "reject"]:
            return jsonify(errno=RET.PARAMERR, errmsg="参数错误")

        news = None
        try:
            news = News.query.get(news_id)
        except Exception as e:
            current_app.logger.error(e)
            return jsonify(errno=RET.DBERR, errmsg="数据库查询错误")

        if not news:
            return jsonify(errno=RET.NODATA, errmsg="未查询到数据")
        # 根据action的状态设置不同的值
        if action == "accept":
            news.status = 0
        else:
            reason = request.json.get("reason")
            if not reason:
                return jsonify(errno=RET.PARAMERR, errmsg="参数错误")
            news.reason = reason
            news.status = -1

        # 保存数据库
        try:
            db.session.commit()
        except Exception as e:
            current_app.logger.error(e)
            db.session.rollback()
            return jsonify(errno=RET.DBERR, errmsg="数据保存失败")
        return jsonify(errno=RET.OK, errmsg="操作成功")


@admin_blu.route("/news_review")
def news_review():
    """新闻审核列表"""
    page = request.args.get("p", 1)
    keywords = request.args.get("keywords", "")
    try:
        page = int(page)
    except Exception as e:
        current_app.logger.error(e)
        page = 1

    news_list = []
    current_page = 1
    total_page = 1

    try:
        filters = [News.status != 0]
        if keywords:
            # 添加关键字搜索选项
            filters.append(News.title.contains(keywords))
        paginate = News.query.filter(*filters).order_by(News.create_time.desc()).paginate(page,
                                                                                          constants.ADMIN_NEWS_PAGE_MAX_COUNT,
                                                                                          False)
        news_list = paginate.items
        current_page = paginate.page
        total_page = paginate.pages
    except Exception as e:
        current_app.logger.error(e)

    news_dict_li = []
    for news in news_list:
        news_dict_li.append(news.to_review_dict())
    data = {
        "news_list": news_dict_li,
        "current_page": current_page,
        "total_page": total_page
    }
    return render_template("admin/news_review.html", data=data)


@admin_blu.route("/user_list")
def user_list():
    """用户列表"""
    page = request.args.get("p", 1)
    try:
        page = int(page)
    except Exception as e:
        current_app.logger.error(e)
        page = 1

    # 设置变量的默认值
    users = []
    current_page = 1
    total_page = 1
    try:
        paginate = User.query.filter(User.is_admin == False).order_by(User.last_login.desc()).paginate(page,
                                                                                                       constants.ADMIN_USER_PAGE_MAX_COUNT,
                                                                                                       False)
        users = paginate.items
        current_page = paginate.page
        total_page = paginate.pages
    except Exception as e:
        current_app.logger.error(e)
    users_list = []
    for user in users:
        users_list.append(user.to_admin_dict())
    data = {
        "user_list": users_list,
        "current_page": current_page,
        "total_page": total_page
    }
    return render_template("admin/user_list.html", data=data)


@admin_blu.route("/user_count")
def user_count():
    """用户统计"""
    total_count = 0
    try:
        total_count = User.query.filter(User.is_admin == False).count()
    except Exception as e:
        current_app.logger.error(e)
    # 月新增数
    mon_count = 0
    t = time.localtime()
    # strptime , 将时间字符串格式转换成datetime格式
    begin_mon_date = datetime.datetime.strptime(("%d-%02d-01" % (t.tm_year, t.tm_mon)), "%Y-%m-%d")
    try:
        mon_count = User.query.filter(User.is_admin == False, User.create_time > begin_mon_date).count()
    except Exception as e:
        current_app.logger.error(e)

    # 日新增数
    day_count = 0
    begin_day_date = datetime.datetime.strptime(("%d-%02d-%02d" % (t.tm_year, t.tm_mon, t.tm_mday)), "%Y-%m-%d")
    try:
        day_count = User.query.filter(User.is_admin == False, User.create_time > begin_day_date).count()
    except Exception as e:
        current_app.logger.error(e)
    # strftime 将datetime格式转换成时间字符串格式
    t = datetime.datetime.now().strftime("%Y-%02m-%d")
    now_date = datetime.datetime.strptime(t, "%Y-%m-%d")
    # 存放活跃日期
    active_date = []
    # 存放活跃人数
    active_count = []
    for i in range(0, 31):
        begin_day = now_date - datetime.timedelta(days=i)
        end_date = now_date - datetime.timedelta(days=(i - 1))
        count = User.query.filter(User.is_admin == False, User.last_login >= begin_day,
                                  User.last_login < end_date).count()
        active_date.append(begin_day.strftime("%Y-%m-%d"))
        active_count.append(count)

    active_count.reverse()
    active_date.reverse()
    data = {
        "total_count": total_count,
        "mon_count": mon_count,
        "day_count": day_count,
        "active_date": active_date,
        "active_count": active_count
    }

    return render_template("admin/user_count.html", data=data)


@admin_blu.route("/login", methods=["get", "post"])
def admin_login():
    """管理员登录界面"""
    if request.method == "GET":
        # 进入管理员用户登录界面时候判断管理员是否有登录，有的话直接跳转到管理员主页
        user_id = session.get("user_id", None)
        is_admin = session.get("is_admin", False)
        if user_id and is_admin:
            return redirect(url_for("admin.admin_index"))
        return render_template("admin/login.html")

    username = request.form.get("username")
    password = request.form.get("password")

    if not all([username, password]):
        return render_template("admin/login.html", errmsg="缺少参数")

    try:
        user = User.query.filter(User.mobile == username, User.is_admin == True).first()
    except Exception as e:
        current_app.logger.error(e)
        return render_template("admin/login.html", errmsg="用户信息查询失败")
    if not user:
        return render_template("admin/login.html", errmsg="未查询到用户信息")
    if not user.check_password(password):
        return render_template("admin/login.html", errmsg="用户名或密码错误")

    session["user_id"] = user.id
    session["mobile"] = user.mobile
    session["nick_name"] = user.nick_name
    session["is_admin"] = user.is_admin

    return redirect(url_for("admin.admin_index"))


@admin_blu.route("/index")
@user_login_data
def admin_index():
    """后台管理主页"""
    user = g.user
    data = {
        "user_info": user.to_dict() if user else None
    }
    return render_template("admin/index.html", data=data)
