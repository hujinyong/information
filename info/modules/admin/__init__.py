#!/usr/bin/env python
# -*- coding:utf-8 -*-
from flask import Blueprint
from flask import redirect
from flask import request
from flask import session
from flask import url_for

admin_blu = Blueprint("admin",__name__,url_prefix="/admin")

from . import views


# 在管理员蓝图前创建一个请求之前的钩子函数来判断
@admin_blu.before_request
def check_admin():
    # user_id = session.get("user_id")
    is_admin = session.get("is_admin",False)
    if not is_admin and not request.url.endswith(url_for("admin.admin_login")):
        return redirect("/")
