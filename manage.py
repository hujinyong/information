from flask import session

from flask_migrate import Migrate, MigrateCommand
from flask.ext.script import Manager
from info import create_app, db
from info import models
from info.models import User

config_name = 'developement'
app = create_app(config_name)
# 将app对象和db进行关联
Migrate(app, db)
manager = Manager(app)
# 将数据库迁移命令绑定到命令行
manager.add_command("db", MigrateCommand)


@manager.option("-n", "-name", dest="username")
@manager.option("-p", "-password", dest="password")
def createsuperuser(username, password):
    """创建管理员账户"""
    if not all([username, password]):
        print("参数不足")
        return
    user = User()
    user.mobile = username
    user.nick_name = username
    user.password = password
    user.is_admin = True
    try:
        db.session.add(user)
        db.session.commit()
    except Exception as e:
        print(e)
        db.session.rollback()


if __name__ == '__main__':
    # print(app.url_map)
    manager.run()
