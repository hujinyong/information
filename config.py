# 将文件配置都抽取到单独的一个文件中，进行解耦
import logging
from redis import StrictRedis


class Config(object):

    # 配置连接数据库
    SQLALCHEMY_DATABASE_URI = "mysql://root:mysql@127.0.0.1:3306/information"
    # 数据库修改不追踪
    SQLALCHEMY_TRACK_MODIFICATIONS= False

    #  查询时候显示原始SQL语句
    # SQLALCHEMY_ECHO = True

    # 实现请求结束之后进行数据自动提交
    SQLALCHEMY_COMMIT_ON_TEARDOWN = True
    # 设置redis常量
    HOST_IP = '127.0.0.1'
    PORT = 6379

    # 默认日志等级
    LOG_LEVEL = logging.DEBUG
    # 设置session配置
    # 使用flask_WTF需要配置此项用来生成加密令牌
    SECRET_KEY = "EjpNVSNQTyGi1VvWECj9TvC/+kq3oujee2kTfQUs8yCM6xX9Yjq52v54g+HVoknA"
    # 指定数据库为redis
    SESSION_TYPE = "redis"
    # 取消不过期保持
    SESSION_PERMANENT = False
    # 设置过期时间
    PERMANENT_SESSION_LIFETIME = 86400 * 2
    # 设置保存session的redis数据库
    SESSION_REDIS = StrictRedis(host=HOST_IP, port=PORT)


# 创建不同的类，满足不同环境下的配置需要
class DevelopementConfig(Config):
    """开发环境配置"""
    DEBUG = True


class ProductionConfig(Config):
    """生产环境配置"""
    HOST_IP = '127.0.0.1'
    PORT = 6379
    LOG_LEVEL = logging.ERROR


class TesetingConfig(Config):
    """测试环境配置"""
    DEBUG = True

# 创建一个配置字典来保存各项配置的引用
config_dict = {
    "developement": DevelopementConfig,
    "production": ProductionConfig,
    "testing": TesetingConfig
}


